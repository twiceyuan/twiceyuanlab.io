title: "在 Parallels Desktop 10 上 安装的 Fedora 上安装 Oracle 11gR2"
date: 2015-03-19 11:14:09
tags: [oracle]

---

在 Fedora 上安装 Parallels Tools 有些小问题。需要在此之前安装一些依赖：

[参考来源](http://kb.parallels.com/en/118876)

<!--more-->


> 权威指南： http://oracle-base.com/articles/11g/oracle-db-11gr2-installation-on-fedora-21.php
> 从中午到晚上总算折腾好了，不得不说坑还是挺多的，建议直接按照上面链接来一步一步做。其他版本理论上也都可以，主要是安装好依赖、环境变量和用户路径权限。我使用的是Fedora 21 LXDE版本的桌面环境。另外看到网上也有在 Docker 里安装好的，也未尝不是一个好的选择。