title: TIP003
tags:
  - tips
date: 2015-04-03 08:24:51
---

**True Nobility**

In a calm sea every man is a pilot. 

But all sunshine without shade, all pleasure without pain, is not life at all. Take the lot of the happiest—it is a tangled yarn. Bereavements and blessings, one following another, make us sad and blessed by turns. Even death itself makes life more loving. Men come closest to their true selves in the sober moments of life, under the shadows of sorrow and loss. 

<a id="more"></a>

In the affairs of life or of business, it is not intellect that tells so much as character, not brains so much as heart, not genius so much as self-control, patience, and discipline, regulated by judgment. 

I have always believe that the man who has begun to live more seriously within begins to live more simply without. In an age of extravagance and waste, I wish I could show to the world how few the real wants of humanity are. 

To regret one’s errors to the point of not repeating them is true repentance. There is nothing noble in being superior to some other man. The true nobility is in being superior to your previous self. 

* * *

风平浪静的大海，每个人都是领航员。 

但是，只有阳光而无阴影，只有欢乐而无痛苦，那就不是人生。以最幸福的人的生活为例—它是一团纠缠在一起的麻线。丧母之痛和幸福祝愿彼此相接，是我们一会伤心，一会高兴，甚至死亡本身也会使生命更加可亲。在人生的清醒的时刻，在哀痛和伤心的阴影之下，人们真实的自我最接近。 

在人生或者职业的各种事务中，性格的作用比智力大得多，头脑的作用不如心情，天资不如由判断力所节制着的自制，耐心和规律。 

我始终相信，开始在内心生活得更严肃的人，也会在外表上开始生活得更朴素。在一个奢华浪费的年代，我希望能向世界表明，人类真正需要的的东西是非常之微少的。 

悔恨自己的错误，而且力求不再重蹈覆辙，这才是真正的悔悟。优于别人，并不高贵，真正的高贵应该是优于过去的自己。 

![](/2015/03/30/TIPS003/tip003.jpg)