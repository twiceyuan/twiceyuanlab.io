title: JetBrains 家应用字体渲染问题
date: 2015-10-02 10:00:32
tags: [OS X, Android Studio]

---
昨天安装了新的系统 El Capitan，升级后发现 Android Studio 中字体很虚，但是其他应用又没有问题，在 v2ex 上提问后有人说 java 环境问题：因为 JetBrains 的 IDE 基本都是用 Java 开发的（包括 Intellij IDEA、php Storm、Android Studio 等），使用苹果提供的 java 环境里有针对系统的字体渲染，而使用 oracle 提供的 jre 就没有。所以同样有字体发虚问题的朋友请到 Apple 官网下载 java 环境即可（R 屏可无视）