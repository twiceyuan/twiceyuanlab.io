title: Linux系统应用与开发02
date: 2013-03-05 21:14:10
tags: [Linux,课堂笔记]

---
#<center>上机实验二</center>
<!--more-->
###在 Linux 下完成常见任务
###【实验目的】
通过在 Linux 系统环境中完成常见的操作任务,熟悉 Linux 系统的基本用法。

###【实验内容】 
* 系统的登录、注销、关机与重启 
* Linux 图形界面基本应用
* Linux 系统基本命令及其应用
* Linux 系统环境中基本的文件与目录操作 
* Linux 系统文件权限设置
* Linux 系统中的文件链接及其应用
###【实验步骤】
* 登录系统(普通用户)。
* 分别应用 man、info 查看常用的 ls、ifconfig 命令的用法,熟悉 man、info的用法。
* (教材第 74 页第 8 题)将主目录下的.bashrc 文件复制到/tmp 下,并重命名为 bashrc,用命令实现上述过程。
* 在主目录下创建一个名为 wang 的目录,并在该目录中用 touch test.txt创建一个文本文件。
* (教材第 75 页第 9 题)将主目录中的 wang 目录做归档压缩,压缩后生成 wang.tar.gz 文件,并将此文件保存到/home 目录下,用命令实现上述过程。
* (教材第 75 页第 14 题)在用户主目录下新建目录 software,并将路径/etc 下所有以 h 开头的文件及目录复制到 software 中,用命令实现上述过程。
* (教材第 75 页第 13 题)在根目录下创建目录 gdc,并设置权限为 gdc
的文件主具有读写执行权限,与文件主同组用户可读写,其他任何用户则只能读。 
* 在主目录下的 wang 目录中分别创建一个到第 7 题所建 software 目录的 硬链接与符号链接,分别命令为 software_h 和 software_s,然后用 ls -l 查看两个
链接的属性,熟悉相关含义。
###【实验时间】
2 学时
