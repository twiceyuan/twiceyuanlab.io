title: RxAndroid学习路线
tags:
  - Android
  - RxJava
  - RxAndroid

date: 2015-09-03 16:49:56

---

最近在看函数响应式编程，感觉学习过程酸爽不过确实在异步操作等方面相对于传统方式优雅到爆，这里分享一些国内外不错的文档。

1.  [官方文档](http://reactivex.io/intro.html)
2.  [官方文档中文翻译](https://www.gitbook.com/book/mcxiaoke/rxdocs/details)
3.  [NotRxJava懒人专用指南](http://www.devtf.cn/?p=323)
4.  [深入浅出RxJava（一：基础篇）](http://blog.csdn.net/lzyzsd/article/details/41833541)
5.  [深入浅出RxJava（二：操作符）](http://blog.csdn.net/lzyzsd/article/details/44094895)
6.  [深入浅出RxJava（三：响应式的好处）](http://blog.csdn.net/lzyzsd/article/details/44891933)
7.  [深入浅出RxJava（四：在Android中使用响应式编程）](http://blog.csdn.net/lzyzsd/article/details/45033611)
