title: 为网站配置 Safari 图标
tags:
  - tips
date: 2015-04-19 16:24:57
---

发现很多网站，比如微博、Github、知乎等，在 Safari 的书签图标都是高清无码的。而且这个和 favicon 不是同一个图片，经过搜索，这个原来是为 Safari 单独适配的。

方法很简单，把一个高清的图标放到网站根目录下，改名为：apple-touch-icon.png 就行了。