title: 经典算法合集
date: 2014-03-10 12:17:24
tags: 算法
---
##排序 

排序算法:http://baike.baidu.com/view/297739.htm 

冒泡排序法:http://baike.baidu.com/view/1313793.htm 

起泡法:http://baike.baidu.com/view/174304.htm 

鸡尾酒排序:http://baike.baidu.com/view/1981861.htm 

桶排序:http://baike.baidu.com/view/1784217.htm 
<!--more-->
计数排序:http://baike.baidu.com/view/1209480.htm 

归并排序:http://baike.baidu.com/view/90797.htm 

排序二叉树:http://baike.baidu.com/view/922220.html 

鸽巢排序:http://baike.baidu.com/view/2020276.htm 

基数排序:http://baike.baidu.com/view/1170573.htm 

选择排序法:http://baike.baidu.com/view/1575807.htm 

希尔排序:http://baike.baidu.com/view/178698.htm 

堆排序:http://baike.baidu.com/view/157305.htm 

快速排序算法:http://baike.baidu.com/view/19016.htm 

插入排序法:http://baike.baidu.com/view/1443814.htm 

树形选择排序:http://baike.baidu.com/view/3108940.html 

------------------------------------------------------------

##搜索 

深度优先搜索:http://baike.baidu.com/view/288277.htm 

宽度优先搜索:http://baike.baidu.com/view/825760.htm 

启发式搜索:http://baike.baidu.com/view/1237243.htm 

蚁群算法:http://baike.baidu.com/view/539346.htm 

遗传算法:http://baike.baidu.com/view/45853.htm 

------------------------------------------------------------ 

##计算几何 

凸包:http://baike.baidu.com/view/707209.html 

------------------------------------------------------------ 

##图论 

哈夫曼编码:http://baike.baidu.com/view/95311.htm 

二叉树遍历:http://baike.baidu.com/view/549587.html 

最短路径:http://baike.baidu.com/view/349189.htm 

Dijkstra算法:http://baike.baidu.com/view/7839.htm 

A*算法:http://baike.baidu.com/view/7850.htm 

SPFA算法:http://baike.baidu.com/view/682464.html 

Bellman-Ford算法:http://baike.baidu.com/view/1481053.htm 

floyd-warshall算法:http://baike.baidu.com/view/2749461.htm 

Dijkstra算法:http://baike.baidu.com/view/7839.htm 

最小生成树:http://baike.baidu.com/view/288214.htm 

Prim算法:http://baike.baidu.com/view/671819.html 

网络流:http://baike.baidu.com/view/165435.html 

------------------------------------------------------------ 

##动态规划 

动态规划:http://baike.baidu.com/view/28146.htm 

哈密顿图:http://baike.baidu.com/view/143350.html 

递推:http://baike.baidu.com/view/3783120.htm 

------------------------------------------------------------ 

##动态规划优化 

优先队列:http://baike.baidu.com/view/1267829.htm 

单调队列:http://baike.baidu.com/view/3771451.htm 

四边形不等式:http://baike.baidu.com/view/1985058.htm 

------------------------------------------------------------ 

##其他 

随机化算法:http://baike.baidu.com/view/1071553.htm 

递归:http://baike.baidu.com/view/96473.htm 

穷举搜索法:http://baike.baidu.com/view/1189634.htm 

贪心算法:http://baike.baidu.com/view/112297.htm 

分治法:http://baike.baidu.com/view/1583824.htm 

迭代法:http://baike.baidu.com/view/649495.htm 

加密算法:http://baike.baidu.com/view/155969.htm 

回溯法:http://baike.baidu.com/view/45.htm 

弦截法:http://baike.baidu.com/view/768310.htm 

迭代法:http://baike.baidu.com/view/649495.htm 

背包问题:http://baike.baidu.com/view/841810.htm 

http://baike.baidu.com/view/1731915.htm 

八皇后问题:http://baike.baidu.com/view/698719.htm 

百鸡问题:http://baike.baidu.com/view/367996.htm 

二分法:http://baike.baidu.com/view/75441.htm 

kmp算法:http://baike.baidu.com/view/659777.html 

遗传算法:http://baike.baidu.com/view/45853.htm 

矩阵乘法:http://www.douban.com/group/topic/12416781/edit 

Floyd算法:http://baike.baidu.com/view/14495.html 

路由算法:http://baike.baidu.com/view/2276401.html 

ICP算法:http://baike.baidu.com/view/1954001.html 

约瑟夫环:http://baike.baidu.com/view/717633.htm 

约瑟夫问题:http://baike.baidu.com/view/213217.htm 

AVL树:http://baike.baidu.com/view/414610.htm 

红黑树:http://baike.baidu.com/view/133754.htm 

退火算法:http://baike.baidu.com/view/335371.htm#sub335371 

并查集:http://baike.baidu.com/view/521705.htm 

线段树:http://baike.baidu.com/view/670683.htm 


左偏树:http://baike.baidu.com/view/2918906.htm 

Treap:http://baike.baidu.com/view/956602.htm 

Trie树:http://baike.baidu.com/view/1436495.html 

RMQ:http://baike.baidu.com/view/1536346.htm 

LCA :http://baike.baidu.com/view/409050.htm 

矩阵乘法:http://baike.baidu.com/view/2455255.htm 

高斯消元:http://baike.baidu.com/view/33268.html 

银行家算法:http://baike.baidu.com/view/93075.htm 