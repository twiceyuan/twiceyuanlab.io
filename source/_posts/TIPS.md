title: TIPS
date: 2015-09-24 15:05:54
tags: tips
---

As the Secretary General of the United Nations, an organizations of the 147 member states who represent almost all of the human inhabitants of the planet earth. I send greetings on behalf of the people of our planet. We step out of our solar system into the universe seeking only peace and friendship, to teach if we are called upon, to be taught if we are fortunate. We know full well that our planet and all its inhabitants are but a small part of the immense universe that surrounds us and it is with humility and hope that we take this step.

作为联合国秘书长，一个拥有147个成员国，代表了几乎所有地球居民的组织， 我向你们送去全人类的问候。我们走出太阳系的家门进入这宇宙，只为寻找和平和友谊，抑或应邀倾囊相授，抑或有幸聆听教诲。我们很清楚，我们星球及上面的所有生灵，只不过是这广阔宇宙中微小的一部分。我们谦卑而满怀希望地迈出这一步。

—— 1977 库尔特·瓦尔德海姆