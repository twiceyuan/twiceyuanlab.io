title: Android SDK 国内的镜像站
date: 2015-02-16 15:14:27
tags: Android

---

国内更新Android SDK不通畅，可以尝试使用设置镜像地址。

大连东软的 Android SDK 镜像地址：`mirrors.neusoft.edu.cn:80`

UPDATE：最近国内有服务器了，是北京的，所以不需要镜像了大家可以使用 ping.chinaz.com 来看一下哪个 IP 访问最快来修改自己的 hosts。不过大部分地区应该可以直接访问了。
